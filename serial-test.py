#!/usr/bin/env python

# based on tutorials:
#   http://www.roman10.net/serial-port-communication-in-python/
#   http://www.brettdangerfield.com/post/raspberrypi_tempature_monitor_project/

import serial, time

SERIALPORT = "/dev/ttyUSB0"
BAUDRATE = 9600

ser = 0

def open_port(port, baud):
    global ser
    try:
        ser = serial.Serial(port, baud)

        ser.bytesize = serial.EIGHTBITS #number of bits per bytes
        ser.parity = serial.PARITY_NONE #set parity check: no parity
        ser.stopbits = serial.STOPBITS_ONE #number of stop bits
        #ser.timeout = None          #block read
        #ser.timeout = 0             #non-block read
        ser.timeout = 1              #timeout block read
        ser.xonxoff = False     #disable software flow control
        ser.rtscts = False     #disable hardware (RTS/CTS) flow control
        ser.dsrdtr = False       #disable hardware (DSR/DTR) flow control
        ser.writeTimeout = 0     #timeout for write

        print('Starting Up Serial Monitor')
        print(ser)

    except Exception as e:
        print ("error openning serial port: " + str(e))
        return -1

    if ser.isOpen():
        return 1;

    return 0

def write_text(text):
    try:
        ser.flushInput() #flush input buffer, discarding all its contents
        ser.flushOutput()#flush output buffer, aborting current output

        ser.write(text)
        print("write data: ", text)

    except Exception as e:
        print("error reading...: " + str(e))
        return -1

    return 1


def read_text():
    numberOfLine = 0
    text = ""

    try:
        while True:
            response = ser.readline()
            print("read data: " + response.decode())
            text += response.decode()

            numberOfLine = numberOfLine + 1
            if (numberOfLine >= 3):
                break

    except Exception as e:
        print("error reading...: " + str(e))
        return ""

    return text


def close_port():
    ser.close()


def query_co2():
    global ser

    try:
        query_co2 = [0xff, 0x01, 0x86, 0x00, 0x00, 0x00, 0x00, 0x00, 0x79]
        query_calib = [0xff, 0x01, 0x87, 0x00, 0x00, 0x00, 0x00, 0x00, 0x78]
        query = query_co2

        query_arr = bytes(query)
        print(query_arr)

        ser.write(query_arr)

        ser.flush()

        while (ser.out_waiting):
            pass

        print("CO2 query sent...: ")

        resp_arr = ser.read(100)

        print("CO2 response rcv'd...: " + str(query_arr))
        print("CO2 response rcv'd...: " + str(resp_arr))

        co2_conc = -1
        if (resp_arr[0] == 0xff and resp_arr[1] == 0x86):
            co2_conc = 256*resp_arr[2] + resp_arr[3]

        print("CO2: " + str(co2_conc))
        return len(resp_arr)

    except Exception as e:
        print("error querying CO2...: " + str(e))
        return -1



if (open_port(SERIALPORT, BAUDRATE)):

    time.sleep(0.5)
#    write_text(b'Ahoj svete!\n\r')
#    read_text()

    while (True):
        print(query_co2())
        time.sleep(5)

    close_port()
else:
    print("cannot open serial port ")

print("Done")
