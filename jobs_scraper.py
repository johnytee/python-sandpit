import requests
from bs4 import BeautifulSoup
import re
import numpy as np
import pandas as pd
import datetime

# https://www.jobs.cz/prace/plzen/is-it-vyvoj-aplikaci-a-systemu/?page=1&locality%5Bradius%5D=0
url_start = "https://www.jobs.cz/prace/plzen/is-it-vyvoj-aplikaci-a-systemu/?page="
url_start = "https://www.jobs.cz/prace/"
url_end = "locality%5Bradius%5D=0"
url_tag = "is-it-vyvoj-aplikaci-a-systemu"
url_location = "plzen"

# global variables
ad_list = list()
comp_pos = 0
today = datetime.datetime.now()
yesterday = today - datetime.timedelta(days=1)


def set_location(location, radius):
    global url_location
    global url_end

    url_location = location
    url_end = "locality%5Bradius%5D=" + str(radius)
    return


def set_tag(tag):
    global url_tag

    url_tag = tag
    return


def get_jobs_cz_positions_no():
    global url_location
    global url_start
    global url_end
    global url_tag

    # https://realpython.com/beautiful-soup-web-scraper-python/

    url = url_start + url_location + "/" + url_tag + "/?" + url_end

    page = requests.get(url)

    soup = BeautifulSoup(page.content, 'html.parser')

    results = soup.find('body')

    job_elems = results.find('h1', class_='headline headline--secondary')

    # https://regex101.com/
    pattern = r'\b\d+\b'
    count = re.findall(pattern, job_elems.get_text().strip())

    return int(count[0])


def convert_crazy_jobs_date_format(day_month):
    day_month[0] = day_month[0].replace('.', '')

    month_dict = {
        "ledna": 1,
        "února": 2,
        "března": 3,
        "dubna": 4,
        "května": 5,
        "června": 6,
        "července": 7,
        "srpna": 8,
        "září": 9,
        "října": 10,
        "listopadu": 11,
        "prosince": 12
    }

    day_month[1] = month_dict[day_month[1]]

    return day_month


def get_date_from_updated(updated):
    # Conversion of date information from text on Webpage to actual date
    # TODO: Needs to be rewritten, would not work around year's end
    global today
    global yesterday

    if (updated == 'Přidáno dnes'):
        datum = today.strftime("%d.%m.%Y")
    elif (updated == 'Přidáno včera'):
        datum = yesterday.strftime("%d.%m.%Y")
    else:
        dm = convert_crazy_jobs_date_format(updated.split())
        datum = datetime.datetime(int(today.strftime("%Y")), int(dm[1]), int(dm[0])).strftime("%d.%m.%Y")

    try:
        datum
    except NameError:
        datum = None

    if datum is None:
        retval = updated
    else:
        retval = datum

    return retval


def get_jobs_cz_positions_list(page_num):
    # https://realpython.com/beautiful-soup-web-scraper-python/

    global ad_list
    global count

    url = url_start + url_location + "/" + url_tag + "/?page=" + str(page_num) + "&" + url_end

    page = requests.get(url)

    soup = BeautifulSoup(page.content, 'html.parser')

    results = soup.find('body')

    local_count = 0
    job_elems = results.find_all('div', class_='standalone search-list__item')
    for job_elem in job_elems:
        if job_elem.get('data-position-id') is not None:
            position = job_elem.find(
                'h3', class_='search-list__main-info__title').get_text().strip()
            company = job_elem.find(
                'div', class_='search-list__main-info__company').get_text().strip()
            updated = job_elem.find('span', class_='label-added').get_text().strip()
            count += 1
            local_count += 1

            ad_list.append((count, page_num, company, position, get_date_from_updated(updated)))

    return local_count


def convert_to_dfs():
    global df_comp_pos_stats
    global df_ad_list
    global ad_list
    global comp_pos

    np_ad_list = np.array(ad_list)
#    comp_pos = np.transpose(comp_pos)

    column_values = ['Pořadí', 'Strana', 'Firma', 'Pozice', 'Poslední aktualizace']
    df_ad_list = pd.DataFrame(data=np_ad_list, columns=column_values)

    print(df_ad_list)

    column_values = ['Firma', 'Počet pozic']
    df_comp_pos_stats = pd.DataFrame(data=comp_pos, columns=column_values)

    print(df_comp_pos_stats)

    filename = r'Pilsen_stat.xlsx'
    with pd.ExcelWriter(filename) as writer:
        df_ad_list.to_excel(writer, sheet_name='Adverts', index=False)
        df_comp_pos_stats.to_excel(writer, sheet_name='Companies', index=False)


def get_stats():
    global ad_list
    global comp_pos

    np_array = np.array(ad_list)
    comp_pos = np.array(np.unique(np_array[:, 2], return_counts=True))
    comp_pos = np.transpose(comp_pos)


if __name__ == '__main__':
    print('Jobs.cz má u tagu %s %d nabídek v Plzni' % (url_tag, get_jobs_cz_positions_no()))

    count = 0
    page = 1
    while (get_jobs_cz_positions_list(page)):
        page += 1

    get_stats()

    convert_to_dfs()
