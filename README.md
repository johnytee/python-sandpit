# Python experiment sandpit

This project serves as a project in GitLab for initial "incubation" of new ideas.

1. Jobs.cz scraper - statistics
https://www.jobs.cz/prace/plzen/is-it-vyvoj-aplikaci-a-systemu/?locality%5Bradius%5D=0


[Learn more about creating GitLab projects.](https://docs.gitlab.com/ee/gitlab-basics/create-project.html)

# Code guidelines

PEP8

Atom:

Atom Python Autopep8
Uses autopep8 installed to format python code according to pep8 guidelines.

Install
Make sure to have autopep8 installed. Install it by running:
`pip install autopep8`


