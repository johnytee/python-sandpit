#!/usr/bin/env python

# based on tutorials:
#   http://www.roman10.net/serial-port-communication-in-python/
#   http://www.brettdangerfield.com/post/raspberrypi_tempature_monitor_project/

import serial, time

SERIALPORT = "COM5"
BAUDRATE = 115200
IP_ADDRESS_MQTT = ""

wifi_credentials = {
    "Turris_2G":        "heslo",
    "PlzenecNET_Vlcek": "Domanik21"
}

ser = 0

at_at = "AT\r\n"
at_ate = "ATE0\r\n"
at_rst = "AT+RST\r\n"
at_gmr = "AT+GMR\r\n"
at_cwmode_s = "AT+CWMODE_CUR=1\r\n"
at_cwmode_q = "AT+CWMODE_CUR?\r\n"
at_cwlap = "AT+CWLAP\r\n"
at_cwjap_q = "AT+CWJAP_CUR?\r\n"
at_cwjap_s = 'AT+CWJAP_CUR="PlzenecNET_Vlcek","Domanik21"\r\n'
at_cipdomain = 'AT+CIPDOMAIN="test.mosquitto.org"\r\n'

def ser_open_port(port, baud):
    global ser
    try:
        ser = serial.Serial(port, baud)

        ser.bytesize = serial.EIGHTBITS #number of bits per bytes
        ser.parity = serial.PARITY_NONE #set parity check: no parity
        ser.stopbits = serial.STOPBITS_ONE #number of stop bits
        #ser.timeout = None          #block read
        #ser.timeout = 0             #non-block read
        ser.timeout = 2              #timeout block read
        ser.xonxoff = False     #disable software flow control
        ser.rtscts = False     #disable hardware (RTS/CTS) flow control
        ser.dsrdtr = False       #disable hardware (DSR/DTR) flow control
        ser.writeTimeout = 0     #timeout for write

        print('Serial port opened')
        print(ser)

    except Exception as e:
        print ("Error openning serial port: " + str(e))
        return -1

    if ser.isOpen():
        return 1;

    return 0


def wzf_is_present():
    # Send AT and check response
    # Send: AT, Confirm: OK, Ignore: "CRLF"
    query(at_at, ["OK"], ["\r\n", "", "AT"])

    return 1

def wzf_dns_resolve(host_name):
    query_cip(at_cipdomain, ["OK"], ["\r\n", "", "AT"])

    return 1


def wzf_set_init():
    # Set static init
    query(at_rst, ["OK", "ready"], ["\r\n", "WIFI CONNECTED", "WIFI GOT IP", "", "AT+RST"])
    query(at_ate, ["OK"], ["\r\n", "ATE0", ""])
    query(at_cwmode_s, ["OK"], ["\r\n", ""])

    return 1


def wzf_connect_wifi():
    # Connect to AP
    ser.timeout = 10
    query(at_cwjap_s, ["OK", "WIFI CONNECTED", "WIFI GOT IP"], ["\r\n", "", "WIFI DISCONNECT"])
    ser.timeout = 2

    return 1


def wzf_init_mqtt():
    # MQTT broker connection
    # Topics pub/sub

    return 0


def ser_read_lines():
    line = ""
    lines = []
    state = 0

    while True:
        char = ser.read(1)
        if len(char) == 1:
            if state in [0,2]:
                if char == b'\r':
                    state = 1
                    continue
            else:
                if state == 1:
                    if char == b'\n':
                        state = 2
                        lines.append(line)
                        line = ""
                    else:
                        state = 0

            if char not in [b'\r', b'\n']:
                line = line + char.decode("ascii")
        else:
            if len(line):
                lines.append(line)

            break;

    return(lines)


def close_port():
    ser.close()


def query(query, confirm, ignore):
    global ser

    query_arr = bytes(query, "ascii")
    print(query_arr)

    ser.write(query_arr)
    try:
        ser.flush()

        while (ser.out_waiting):
            pass

        lines = ser_read_lines()
        count = 0
        confirmed = 0
        for line in lines:
            if line in confirm:
                confirmed = confirmed + 1
#                print("{} - confirming".format(count))

            if line in ignore:
#                print("{} - ignoring".format(count))
                pass

            if line not in ignore and line not in confirm:
                print("ERROR: Unexpected result")

#            print("{}: {}".format(count, line))
#            print("{}: {}".format(count, bytes(line, "ascii")))

            count = count + 1

        return (confirmed >= len(confirm))

    except Exception as e:
        print("error querying CO2...: " + str(e))
        return -1

def query_cip(query, confirm, ignore):
    global ser

    query_arr = bytes(query, "ascii")
    print(query_arr)

    ser.write(query_arr)
    ip_addr = ""
    try:
        ser.flush()

        while (ser.out_waiting):
            pass

        lines = ser_read_lines()
        count = 0
        confirmed = 0
        for line in lines:
            if line in confirm:
                confirmed = confirmed + 1
#                print("{} - confirming".format(count))

            if line in ignore:
#                print("{} - ignoring".format(count))
                pass

            if line not in ignore and line not in confirm:
                print(line)
                if line.find("+CIPDOMAIN:") == 0:
                    IP_ADDRESS_MQTT = line[12:len(line)-1]
                    print("IP ADDR: {}".format(IP_ADDRESS_MQTT))
                else:
                    print("ERROR: Unexpected result")

#            print("{}: {}".format(count, line))
#            print("{}: {}".format(count, bytes(line, "ascii")))

            count = count + 1

        return (confirmed >= len(confirm))

    except Exception as e:
        print("error querying CIP...: " + str(e))
        return -1


if (ser_open_port(SERIALPORT, BAUDRATE)):

    time.sleep(0.5)

    while (True):
        if (wzf_is_present()):
            print("WizFi present")
            if (wzf_set_init()):
                print("WizFi initialized")
                if (wzf_connect_wifi()):
                    print("WizFi connected to Wifi")
                    if (wzf_dns_resolve("")):
                        print("MQTT broker hostname resolved")
                        if (wzf_init_mqtt()):
                            print("MQTT broker connected")
                        else:
                            print("MQTT broker NOT connected")
                    else:
                        print("MQTT broker hostname NOT resolved")
                else:
                    print("WizFi NOT connected to Wifi")
            else:
                print("WizFi NOT initialized")
        else:
            print("WizFi NOT present")

        time.sleep(10)

    close_port()
else:
    print("cannot open serial port ")

print("Done")
